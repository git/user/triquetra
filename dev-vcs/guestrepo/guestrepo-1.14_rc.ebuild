# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1 mercurial versionator

DESCRIPTION="Mercurial extension for guest repositories (alterative to subrepo)"
HOMEPAGE="https://bitbucket.org/selinc/guestrepo/overview"
SRC_URI=""
EHG_REPO_URI="https://bitbucket.org/pnathan/guestrepo"
EHG_REVISION="v$(replace_version_separator '_' '-' ${PV})"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-vcs/mercurial[${PYTHON_USEDEP}]"

DOCS="README.md"

