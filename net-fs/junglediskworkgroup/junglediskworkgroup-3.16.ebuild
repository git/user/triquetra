# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils

MY_PV="${PV/./}0"

DESCRIPTION="Networked storage/backup using Amazon's S3 service."
HOMEPAGE="http://www.jungledisk.com"
SRC_URI_BASE="https://downloads.jungledisk.com/jungledisk/"
SRC_URI="x86? ( ${SRC_URI_BASE}/${PN}${MY_PV}.tar.gz )
		 amd64? ( ${SRC_URI_BASE}/${PN}64-${MY_PV}.tar.gz ) "

LICENSE="jungledisk-tos"
SLOT="0"
KEYWORDS="-* ~x86 ~amd64"
IUSE=""

RDEPEND="sys-fs/fuse
		 x11-libs/libXinerama
		 x11-libs/libX11
		 x11-libs/gtk+
		 dev-libs/atk
		 dev-libs/glib
		 x11-libs/cairo
		 sys-apps/attr
		 sys-apps/acl
		 x11-libs/libnotify
		 !net-fs/junglediskdesktop
		 !net-fs/junglediskserver"

RESTRICT="binchecks strip"

S="${WORKDIR}/${PN}"

src_install() {
	exeinto /opt/${PN}
	doexe junglediskworkgroup junglediskwg
	dosym /opt/${PN}/junglediskwg /opt/bin/junglediskwg
	dosym /opt/${PN}/junglediskworkgroup /opt/bin/junglediskworkgroup
	dosym /usr/lib/libnotify.so /usr/lib/libnotify.so.1
	insinto /usr/share/pixmaps
	doins junglediskworkgroup.png
	dodoc INSTALL
	make_desktop_entry /opt/bin/${PN} "Jungle Disk Wokgroup" \
		/usr/share/pixmaps/${PN}.png "Application;Network;"
}

pkg_postinst() {
	echo
	elog "- You can view the release notes at:"
	elog "  https://www.jungledisk.com/downloads/business/workgroup/releasenotes.aspx"
    echo
	elog "- Jungle Disk attempts to locate your web browser"
	elog "automatically when clicking links in the software."
	elog "If it does not find your preferred web browser, simply"
	elog "create a symlink to your browser named: "
	elog "~/.jungledisk/browser"
	echo
	elog "If you have configured automatic mounting, your Jungle Disk Workgroup will be mounted"
	elog "at the specified directory (default: ~/jungledisk). You can pass additional "
	elog "FUSE parameters after the mount point in the configuration. For example:"
	echo
	elog "/mnt/jungledisk -o allow_other"
	echo
	elog "By default, the jungledisk volume will only be accessible by the user who"
	elog "mounted it.  If you want to allow root or other users to access the mount you need to use"
	elog "the FUSE options '-o allow_root' or '-o allow_other'. You may also need to set"
	elog "the umask, as in '-o umask=007'."
	echo
	elog "Note that if you are not a root user, you must have permission to use the"
	elog "FUSE filesystem to mount your Jungle Disk Workgroup (see above)."
	echo
	elog "Although less reliable than FUSE, you can also access the disk via any WebDAV "
	elog "client:"
	echo
	elog "-- To connect to your Jungle Disk Workgroup in KDE, just open a Konquerer window and type"
	elog "'webdav://localhost:2667/'.  In GNOME, select 'Connect to Server' under the"
	elog "Places menu."
	elog "-- Set the Service type to WebDAV (HTTP)."
	elog "-- Enter 'localhost' in the server and '2667' for the port."
	echo
	elog "Using rsync with Jungle Disk Workgroup:"
	elog "Once running and mounted on the local file system, you can use rsync to easily"
	elog "copy files to and from your S3 storage. Most rsync options should work fine,"
	elog "however for best performance we recommend using the --inplace option which"
	elog "avoids extra renames."
	echo
	elog "You should also use the --times (-t) option so that modification times are"
	elog "replicated and used to detect changes. If you prefer not to replicate"
	elog "modification times, you may want to use --checksum (-c) or --size-only to"
	elog "prevent files from being re-copied on every run."
	echo
	elog "Sample command lines:"
	elog "# copy all files and attributes (times/permissions/owners/links/etc)"
	elog "rsync -a --inplace /src/* /mnt/jungledisk"
	elog "# copy only normal files and preserve modification times"
	elog "rsync -t -r --inplace /src/* /mnt/jungledisk"
	elog "# copy only file data and use size to determine changes"
	elog "rsync -r --size-only --inplace /src/* /mnt/jungledisk"
}
